﻿using Chat.Dal.Pocos;
using Microsoft.EntityFrameworkCore;

namespace Chat.Dal.Interfaces
{
    public interface IDbContext
    {
        DbSet<ApplicationRole> Roles { get; set; }

        DbSet<ApplicationUser> Users { get; set; }

        DbSet<Room> Rooms { get; set; }

        DbSet<Message> Messages { get; set; }

        int SaveChanges();
    }
}
