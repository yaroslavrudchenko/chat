﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Chat.Dal.Pocos
{
    public class ApplicationUser: IdentityUser<int>
    {
        public string DisplayName { get; set; }

        public string Avatar { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
