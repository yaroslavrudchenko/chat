﻿using Chat.Dal.Constants;
using Chat.Dal.Interfaces;
using Chat.Dal.Pocos;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace Chat.Dal.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, IdentityUserClaim<int>,
        IdentityUserRole<int>, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>,
        IDesignTimeDbContextFactory<ApplicationDbContext>,
        IDbContext, IDisposable
    {
        public DbSet<Room> Rooms { get; set; }

        public DbSet<Message> Messages { get; set; }

        public ApplicationDbContext()
            : base()
        {

        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder
                //.UseSqlServer(connectionString);
                .UseSqlServer("Data Source =.\\SQLEXPRESS; Initial Catalog = ChatHub; Integrated Security = true;");

            return new ApplicationDbContext(optionsBuilder.Options);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationRole>().HasData(
                new ApplicationRole { Id = 1, Name = RoleNames.Admin, NormalizedName = RoleNames.Admin, ConcurrencyStamp = Guid.NewGuid().ToString() },
                new ApplicationRole { Id = 2, Name = RoleNames.EducatorTeacher, NormalizedName = RoleNames.EducatorTeacher, ConcurrencyStamp = Guid.NewGuid().ToString() },
                new ApplicationRole { Id = 3, Name = RoleNames.Student, NormalizedName = RoleNames.Student, ConcurrencyStamp = Guid.NewGuid().ToString() });

        }
    }
}
