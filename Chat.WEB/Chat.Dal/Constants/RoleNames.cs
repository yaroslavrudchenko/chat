﻿namespace Chat.Dal.Constants
{
    public class RoleNames
    {
        public const string Admin = nameof(Admin);
        public const string EducatorTeacher = "Educator/teacher";
        public const string Student = nameof(Student);
    }
}
