﻿using Chat.Dal.Context;
using Chat.Dal.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace Chat.Dal.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static IServiceCollection AddDataBaseContext(this IServiceCollection collection, string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException(nameof(connectionString));

            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder
                .UseSqlServer(connectionString)
                .ConfigureWarnings(m => m.Ignore());
            _ = optionsBuilder.UseLoggerFactory(LoggerFactory.Create(builder => { builder.AddConsole(); }));
            optionsBuilder.EnableSensitiveDataLogging();

            collection
                .AddScoped(sProvider => new ApplicationDbContext(optionsBuilder.Options))
                .AddScoped<IDbContext, ApplicationDbContext>(sProvider => sProvider.GetService<ApplicationDbContext>());

            return collection;
        }
    }
}
