﻿using System;
using System.Linq;
using System.IO;
using Chat.Web.Helpers;
using Chat.Web.Models.ViewModels;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Chat.Dal.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Chat.Web.Hubs;
using Chat.Dal.Pocos;
using AutoMapper;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace Chat.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDbContext _dbContext;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly Microsoft.AspNetCore.SignalR.IHubContext<ChatHub> _chatHub;
        private readonly IMapper _mapper;

        public HomeController(
            IDbContext dbContext,
            IWebHostEnvironment hostingEnvironment,
            Microsoft.AspNetCore.SignalR.IHubContext<ChatHub> chatHub,
            IMapper mapper)
        {
            _dbContext = dbContext;
            _hostingEnvironment = hostingEnvironment;
            _chatHub = chatHub;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Account");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFileCollection fileCollection)
        {
            if (fileCollection.Any())
            {
                try
                {
                    var file = fileCollection[0];

                    // Some basic checks...
                    if (file != null && !FileValidator.ValidSize(file.Length))
                        return Json("File size too big. Maximum File Size: 500KB");
                    else if (FileValidator.ValidType(file.ContentType))
                        return Json("This file extension is not allowed!");
                    else
                    {
                        // Save file to Disk
                        var fileName = DateTime.Now.ToString("yyyymmddMMss") + "_" + Path.GetFileName(file.FileName);
                        var filePath = Path.Combine($"{_hostingEnvironment.WebRootPath}/uploads/", fileName);
                        file.CopyTo(new FileStream(filePath, FileMode.Create));

                        string htmlImage = string.Format(
                            "<a href=\"/Content/uploads/{0}\" target=\"_blank\">" +
                            "<img src=\"/Content/uploads/{0}\" class=\"post-image\">" +
                            "</a>", fileName);


                   var senderViewModel = ChatHub._Connections.Where(u => u.Username == User.Identity.Name).FirstOrDefault();
                            var sender = _dbContext.Users.Where(u => u.UserName == senderViewModel.Username).FirstOrDefault();
                            var room = _dbContext.Rooms.Where(r => r.Name == senderViewModel.CurrentRoom).FirstOrDefault();

                            // Build message
                            Message msg = new Message()
                            {
                                Content = Regex.Replace(htmlImage, @"(?i)<(?!img|a|/a|/img).*?>", String.Empty),
                                Timestamp = DateTime.Now.Ticks.ToString(),
                                FromUser = sender,
                                ToRoom = room
                            };

                            _dbContext.Messages.Add(msg);
                            _dbContext.SaveChanges();

                            // Send image-message to group
                            var messageViewModel = _mapper.Map<Message, MessageViewModel>(msg);
                            await _chatHub.Clients.Group(senderViewModel.CurrentRoom).SendAsync("newMessage", messageViewModel);

                        return Json("Success");
                    }

                }
                catch (Exception ex)
                { return Json("Error while uploading" + ex.Message); }
            }

            return Json("No files selected");

        } // Upload

    }
}