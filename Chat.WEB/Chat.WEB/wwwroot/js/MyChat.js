﻿$(function () {
    let model;
    let hub = new signalR.HubConnectionBuilder()
        .withUrl(`/chatHub`)
        .configureLogging(signalR.LogLevel.Trace)
        .build();

    var Model = function () {
        var self = this;
        self.message = ko.observable("");
        self.chatRooms = ko.observableArray([]);
        self.chatUsers = ko.observableArray([]);
        self.chatMessages = ko.observableArray([]);
        self.joinedRoom = ko.observable("");
        self.serverInfoMessage = ko.observable("");
        self.myName = ko.observable("");
        self.myAvatar = ko.observable("");
        self.onEnter = function (d, e) {
            if (e.keyCode === 13) {
                self.sendNewMessage();
            }
            return true;
        }
        self.filter = ko.observable("");
        self.filteredChatUsers = ko.computed(function () {
            if (!self.filter()) {
                return self.chatUsers();
            } else {
                return ko.utils.arrayFilter(self.chatUsers(), function (user) {
                    var displayName = user.displayName().toLowerCase();
                    return displayName.includes(self.filter().toLowerCase());
                });
            }
        });

    };

    Model.prototype = {

        // Server Operations
        sendNewMessage: function () {
            var self = this;
            hub.invoke('Send', self.joinedRoom(), self.message());
            self.message("");
        },

        joinRoom: function () {
            var self = this;
            hub.invoke('Join', self.joinedRoom()).then(function () {
                self.userList();
                self.messageHistory();
            });
        },

        roomList: function () {
            var self = this;
            return hub.invoke('GetRooms').then(function (result) {
                self.chatRooms.removeAll();
                for (var i = 0; i < result.length; i++) {
                    self.chatRooms.push(new ChatRoom(result[i].id, result[i].name));
                }
            });
        },

        userList: function () {
            var self = this;
            hub.invoke('GetUsers', self.joinedRoom()).then(function (result) {
                self.chatUsers.removeAll();
                for (var i = 0; i < result.length; i++) {
                    self.chatUsers.push(new ChatUser(result[i].username,
                        result[i].displayName,
                        result[i].avatar,
                        result[i].currentRoom,
                        result[i].device))
                }
            });

        },

        createRoom: function () {
            var name = $("#roomName").val();
            hub.invoke('CreateRoom', name);
        },

        deleteRoom: function () {
            var self = this;
            hub.invoke('DeleteRoom', self.joinedRoom());
        },

        messageHistory: function () {
            var self = this;
            hub.invoke('GetMessageHistory', self.joinedRoom()).then(function (result) {
                self.chatMessages.removeAll();
                for (var i = 0; i < result.length; i++) {
                    var isMine = result[i].from == self.myName();
                    self.chatMessages.push(new ChatMessage(result[i].content,
                        result[i].timestamp,
                        result[i].from,
                        isMine,
                        result[i].avatar))
                }

                $(".chat-body").animate({ scrollTop: $(".chat-body")[0].scrollHeight }, 1000);

            });
        },

        roomAdded: function (room) {
            var self = this;
            self.chatRooms.push(room);
        },

        roomDeleted: function (id) {
            var self = this;
            var temp;
            ko.utils.arrayForEach(self.chatRooms(), function (room) {
                if (room.roomId() == id)
                    temp = room;
            });
            self.chatRooms.remove(temp);
        },

        userAdded: function (user) {
            var self = this;
            self.chatUsers.push(user)
        },

        userRemoved: function (id) {
            var self = this;
            var temp;
            ko.utils.arrayForEach(self.chatUsers(), function (user) {
                if (user.userName() == id)
                    temp = user;
            });
            self.chatUsers.remove(temp);
        },

        uploadFiles: function () {
            console.log('hello')
            var data = new FormData();
            var file = document.getElementById("btnUpload").files[0];
            data.append("btnUpload", file);

            $.ajax({
                type: "POST",
                url: '/Home/Upload',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                },
                error: function (error) {
                    alert(error);
                }
            });
        }
    };

    // Represent server data
    function ChatRoom(roomId, name) {
        var self = this;
        self.roomId = ko.observable(roomId);
        self.name = ko.observable(name);
    }

    function ChatUser(userName, displayName, avatar, currentRoom, device) {
        var self = this;
        self.userName = ko.observable(userName);
        self.displayName = ko.observable(displayName);
        self.avatar = ko.observable(avatar);
        self.currentRoom = ko.observable(currentRoom);
        self.device = ko.observable(device);
    }

    function ChatMessage(content, timestamp, from, isMine, avatar) {
        var self = this;
        self.content = ko.observable(content);
        self.timestamp = ko.observable(timestamp);
        self.from = ko.observable(from);
        self.isMine = ko.observable(isMine);
        self.avatar = ko.observable(avatar);
    }

            $('ul#room-list').on('click', 'a', function () {
            var roomName = $(this).text();
            model.joinedRoom(roomName);
            model.joinRoom();
            model.chatMessages.removeAll();
            $("input#iRoom").val(roomName);
            $('#room-list a').removeClass('active');
            $(this).addClass('active');
        });

    model = new Model();
    ko.applyBindings(model);

    hub.start().then(() => {
        model.roomList()
            .then(() => {
                setTimeout(function () {
                    if (model.chatRooms().length > 0) {
                        console.log(model.chatRooms()[0].roomId());
                        model.joinedRoom(model.chatRooms()[0].name())
                        model.joinRoom();
                    }
                }, 250);
            });
        model.userList();

            hub.on('NewMessage', function (messageView) {
                var isMine = messageView.From === model.myName();
                var message = new ChatMessage(messageView.content,
                    messageView.timestamp,
                    messageView.from,
                    isMine,
                    messageView.avatar);
                model.chatMessages.push(message);
                $(".chat-body").animate({ scrollTop: $(".chat-body")[0].scrollHeight }, 1000);
            });

            hub.on('GetProfileInfo', function (displayName, avatar) {
                model.myName(displayName);
                model.myAvatar(avatar);
            });

            hub.on('AddUser', function (user) {
                model.userAdded(new ChatUser(user.username,
                    user.displayName,
                    user.avatar,
                    user.currentRoom,
                    user.device));
            });

            hub.on('RemoveUser', function (user) {
                model.userRemoved(user.username);
            });

            hub.on('Join', function (user) {
                model.userRemoved(user.username);
            });

            hub.on('AddChatRoom', function (room) {
                model.roomAdded(new ChatRoom(room.id, room.name));
            });

            hub.on('RemoveChatRoom', function (room) {
                    model.roomAdded(new ChatRoom(room.id, room.name));
            });

            hub.on('OnError', function (message) {
                model.serverInfoMessage(message);
                $("#errorAlert").removeClass("hidden").show().delay(5000).fadeOut(500);
            });

            hub.on('OnRoomDeleted', function (message) {
                model.serverInfoMessage(message);
                $("#errorAlert").removeClass("hidden").show().delay(5000).fadeOut(500);

                if (model.chatRooms().length == 0) {
                    model.joinedRoom("");
                }
                else {
                    // Join to the first room in list
                    $("ul#room-list li a")[0].click();
                }
            });


        })
        .catch(err => {
            console.warn(err);
        });

    hub.onclose(() => {
        console.warn('signalr connection closed')
    });
});