﻿using Microsoft.Extensions.DependencyInjection;
using Chat.Dal.Pocos;
using Chat.Dal.Context;
using Microsoft.AspNetCore.Identity;
using System;

namespace Chat.Web.Infrastracture.Extenstions
{
    public static class ServiceProviderExtensions
    {
        public static IServiceCollection AddAspNetIdentity(this IServiceCollection collection)
        {
            collection
                .AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            collection.ConfigureApplicationCookie(options =>
            {
                options.SlidingExpiration = true;
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(30);
                options.Cookie.Name = "_aid";
                options.LoginPath = "/Account/Login";
                options.AccessDeniedPath = "/Account/AccessDenied";
            });

            return collection;
        }
    }
}
