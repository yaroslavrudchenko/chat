﻿using AutoMapper;
using Chat.Dal.Interfaces;
using Chat.Dal.Pocos;
using Chat.Web.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Chat.Web.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;

        public ChatHub(
            IDbContext dbContext,
            IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        #region Properties
        /// <summary>
        /// List of online users
        /// </summary>
        public readonly static List<UserViewModel> _Connections = new List<UserViewModel>();

        /// <summary>
        /// List of available chat rooms
        /// </summary>
        private readonly static List<RoomViewModel> _Rooms = new List<RoomViewModel>();

        /// <summary>
        /// Mapping SignalR connections to application users.
        /// (We don't want to share connectionId)
        /// </summary>
        private readonly static Dictionary<string, string> _ConnectionsMap = new Dictionary<string, string>();
        #endregion

        public async Task Send(string roomName, string message)
        {
            if (message.StartsWith("/private"))
                await SendPrivate(message);
            else
                await SendToRoom(roomName, message);
        }

        public async Task SendPrivate(string message)
        {
            // message format: /private(receiverName) Lorem ipsum...
            string[] split = message.Split(')');
            string receiver = split[0].Split('(')[1];
            string userId;
            if (_ConnectionsMap.TryGetValue(receiver, out userId))
            {
                // Who is the sender;
                var sender = _Connections.Where(u => u.Username == IdentityName).First();

                message = Regex.Replace(message, @"\/private\(.*?\)", string.Empty).Trim();

                // Build the message
                MessageViewModel messageViewModel = new MessageViewModel()
                {
                    From = sender.DisplayName,
                    Avatar = sender.Avatar,
                    To = "",
                    Content = Regex.Replace(message, @"(?i)<(?!img|a|/a|/img).*?>", String.Empty),
                    Timestamp = DateTime.Now.ToLongTimeString()
                };

                // Send the message
                await Clients.Client(userId).SendAsync("NewMessage", messageViewModel);
                await Clients.Caller.SendAsync("NewMessage", messageViewModel);
            }
        }

        public async Task SendToRoom(string roomName, string message)
        {
            try
            {
                var user = _dbContext.Users.Where(u => u.UserName == IdentityName).FirstOrDefault();
                var room = _dbContext.Rooms.Where(r => r.Name == roomName).FirstOrDefault();

                // Create and save message in database
                Message msg = new Message()
                {
                    Content = Regex.Replace(message, @"(?i)<(?!img|a|/a|/img).*?>", String.Empty),
                    Timestamp = DateTime.Now.Ticks.ToString(),
                    FromUser = user,
                    ToRoom = room
                };
                _dbContext.Messages.Add(msg);
                _dbContext.SaveChanges();

                // Broadcast the message
                var messageViewModel = _mapper.Map<Message, MessageViewModel>(msg);
                await Clients.Group(roomName).SendAsync("NewMessage", messageViewModel);
            }
            catch (Exception)
            {
                await Clients.Caller.SendAsync("onError", "Message not send!");
            }
        }

        public async Task Join(string roomName)
        {
            try
            {
                var user = _Connections.Where(u => u.Username == IdentityName).FirstOrDefault();
                if (user != null && user.CurrentRoom != roomName)
                {
                    // Remove user from others list
                    if (!string.IsNullOrEmpty(user.CurrentRoom))
                        await Clients.GroupExcept(user.CurrentRoom, Context.ConnectionId).SendAsync("RemoveUser", user);

                    // Join to new chat room
                    await Leave(user.CurrentRoom);
                    await Groups.AddToGroupAsync(Context.ConnectionId, roomName);
                    user.CurrentRoom = roomName;

                    // Tell others to update their list of users
                    await Clients.GroupExcept(roomName, Context.ConnectionId).SendAsync("AddUser", user);
                }
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync("onError", "You failed to join the chat room!" + ex.Message);
            }
        }

        private Task Leave(string roomName) => Groups.RemoveFromGroupAsync(Context.ConnectionId, roomName);

        public async Task CreateRoom(string roomName)
        {
            try
            {
                // Accept: Letters, numbers and one space between words.
                Match match = Regex.Match(roomName, @"^\w+( \w+)*$");
                if (!match.Success)
                {
                    await Clients.Caller.SendAsync("onError", "Invalid room name!\nRoom name must contain only letters and numbers.");
                }
                else if (roomName.Length < 5 || roomName.Length > 20)
                {
                    await Clients.Caller.SendAsync("onError", "Room name must be between 5-20 characters!");
                }
                else if (_dbContext.Rooms.Any(r => r.Name == roomName))
                {
                    await Clients.Caller.SendAsync("onError", "Another chat room with this name exists");
                }
                else
                {
                    // Create and save chat room in database
                    var user = _dbContext.Users.Where(u => u.UserName == IdentityName).FirstOrDefault();
                    var room = new Room()
                    {
                        Name = roomName,
                        UserAccount = user
                    };
                    _dbContext.Rooms.Add(room);
                    _dbContext.SaveChanges();

                    if (room != null)
                    {
                        // Update room list
                        var roomViewModel = _mapper.Map<Room, RoomViewModel>(room);
                        _Rooms.Add(roomViewModel);
                        await Clients.All.SendAsync("AddChatRoom", roomViewModel);
                    }
                }
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync("onError", "Couldn't create chat room: " + ex.Message);
            }
        }

        public async Task DeleteRoom(string roomName)
        {
            try
            {
                // Delete from database
                var room = _dbContext.Rooms.Where(r => r.Name == roomName && r.UserAccount.UserName == IdentityName).FirstOrDefault();
                _dbContext.Rooms.Remove(room);
                _dbContext.SaveChanges();

                // Delete from list
                var roomViewModel = _Rooms.First<RoomViewModel>(r => r.Name == roomName);
                _Rooms.Remove(roomViewModel);

                // Move users back to Lobby
                await Clients.Group(roomName).SendAsync("OnRoomDeleted", string.Format("Room {0} has been deleted.\nYou are now moved to the Lobby!", roomName));

                // Tell all users to update their room list
                await Clients.All.SendAsync("RemoveChatRoom", roomViewModel);
            }
            catch (Exception)
            {
                await Clients.Caller.SendAsync("onError", "Can't delete this chat room.");
            }
        }

        public IEnumerable<MessageViewModel> GetMessageHistory(string roomName)
        {
            var messageHistory = _dbContext.Messages.Where(m => m.ToRoom.Name == roomName)
                .Include(x => x.FromUser)
                .OrderByDescending(m => m.Timestamp)
                .Take(20)
                .AsEnumerable()
                .Reverse()
                .ToList();

            return _mapper.Map<IEnumerable<Message>, IEnumerable<MessageViewModel>>(messageHistory);
        }

        public IEnumerable<RoomViewModel> GetRooms()
        {
            if (_Rooms.Count == 0)
            {
                foreach (var room in _dbContext.Rooms.ToArray())
                {
                    var roomViewModel = _mapper.Map<Room, RoomViewModel>(room);
                    _Rooms.Add(roomViewModel);
                }
            }

            return _Rooms.ToList();
        }

        public IEnumerable<UserViewModel> GetUsers(string roomName)
        {
            return _Connections.Where(u => u.CurrentRoom == roomName).ToList();
        }

        #region OnConnected/OnDisconnected
        public override async Task OnConnectedAsync()
        {
            try
            {
                var user = _dbContext.Users.Where(u => u.UserName == IdentityName).FirstOrDefault();

                var userViewModel = _mapper.Map<ApplicationUser, UserViewModel>(user);
                userViewModel.Device = GetDevice();
                userViewModel.CurrentRoom = "";

                if (!_Connections.Any(u => u.Username == IdentityName))
                {
                    _Connections.Add(userViewModel);
                    _ConnectionsMap.Add(IdentityName, Context.ConnectionId);
                }

                await Clients.Caller.SendAsync("GetProfileInfo", user.DisplayName, user.Avatar);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync("onError", "OnConnected:" + ex.Message);
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            try
            {
                var user = _Connections.Where(u => u.Username == IdentityName).First();
                _Connections.Remove(user);

                // Tell other users to remove you from their list
                await Clients.GroupExcept(user.CurrentRoom, Context.ConnectionId).SendAsync("RemoveUser", user);

                // Remove mapping
                _ConnectionsMap.Remove(user.Username);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendAsync("onError", "OnDisconnected: " + ex.Message);
            }

            await base.OnDisconnectedAsync(exception);
        }

        //public override Task OnReconnected()
        //{
        //    var user = _Connections.Where(u => u.Username == IdentityName).FirstOrDefault();
        //    if (user != null)
        //        Clients.Caller.getProfileInfo(user.DisplayName, user.Avatar);

        //    return base.OnReconnected();
        //}
        #endregion

        private string IdentityName
        {
            get { return Context.User.Identity.Name; }
        }

        private string GetDevice()
        {
            var device = Context.GetHttpContext().Request.Headers.FirstOrDefault(
                hV => hV.Key.Equals("Device", StringComparison.OrdinalIgnoreCase) 
                    && hV.Value.ToString().Equals("Desktop", StringComparison.OrdinalIgnoreCase) || hV.Value.ToString().Equals("Mobile", StringComparison.OrdinalIgnoreCase))
                .Value;

            return "Web";
        }
    }
}
